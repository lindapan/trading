
package myartifact;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class TradingStrategyTest {

	@Test
	public void test() throws InterruptedException {
		
		double[] pricesList = {11.0,10.0,11.0,10.0,11.0,10.0,11.0,10.0,9.0,10.0,11.0,13.0,14.0};
		TradingStrategy tradingTest = new TradingStrategy(10, 4);
		tradingTest.create();
		for (double i : pricesList){
			TimeUnit.SECONDS.sleep(2);
			tradingTest.parseValue(i);
				
		}
		
		
	}
	

}

