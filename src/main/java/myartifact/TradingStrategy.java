package myartifact;

public class TradingStrategy {
	

	int longPeriod;
	int shortPeriod;
	MovingAverage longMV;
	MovingAverage shortMV;
	double lMV = 0.0; 
	double sMV = 0.0;
	boolean shortHigher = true;
	
	public TradingStrategy(int l, int s){
		this.longPeriod = l;
		this.shortPeriod = s;
	}
	
	public void create(){
		longMV = new MovingAverage(longPeriod);
		shortMV = new MovingAverage(shortPeriod);
		
	}
	
	public void parseValue(double price){
		lMV = longMV.next(price);
		sMV = shortMV.next(price);	
		System.out.println("lMV is: " + lMV);
		System.out.println("sMV is: " + sMV);
		shortHigher = priceJudge(lMV, sMV, shortHigher);
		
	}
	
	public boolean priceJudge(double lMV, double sMV, boolean previous){
		boolean now;
		if (lMV > sMV){
			now = false;
		}
		else {			
			now = true;
		}
		if (now != previous){
			if (now == false)
			{
				System.out.println("sell");
			}
			else {
				System.out.println("buy");
			}	
		}
		return now;
	
}
//	public static void main(String[] args)
//	{
//		TradingStrategy tradingTest = new TradingStrategy(10, 4);
//		tradingTest.create();
//}
}
