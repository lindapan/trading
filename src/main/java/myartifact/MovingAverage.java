package myartifact;

import java.util.LinkedList;

public class MovingAverage {
	LinkedList<Double> queue;
	int size;
	double avg;
	
	public MovingAverage(int size) {
		this.queue = new LinkedList<Double>();
		this.size = size;
	}
	
	public double next(double val) {
		if(queue.size() < this.size) {
			queue.offer(val);

			double sum = 0.0;

			for (double i: queue){
				sum += i;
			}
			avg = (double)sum/this.size;
			return avg;
		}else {
			double head = queue.poll();
			double minus = (double)head/this.size;
			queue.offer(val);
			double add = (double)val/this.size;
			avg = avg + add - minus;
			return avg;
		}
	}
}
